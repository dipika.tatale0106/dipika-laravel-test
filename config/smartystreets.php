<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| API Keys
	|--------------------------------------------------------------------------
	|
	*/
	'authId' 	=> env('SMARTY_AUTH_ID'),
	'authToken'	=> env('SMARTY_AUTH_TOKEN'),


	/*
	|--------------------------------------------------------------------------
	| Endpoint
	|--------------------------------------------------------------------------
	|
	*/
	// 'endpoint' => 'https://api.smartystreets.com',
	'endpoint' => 'https://us-autocomplete.api.smartystreets.com/suggest',

);
