@extends('layout')

<link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">

<!-- Styles -->
<style>
    html, body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
        height: 100vh;
        margin: 0;
    }

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }

    .content {
        text-align: center;
    }

    .register_form_view {
        border: 1px solid #ced4da;
        width: 660px;
        padding: 15px;
    }

    .title {
        font-size: 24px;
        font-weight: bold;
    }

    .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 13px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }

    .m-b-md {
        margin-bottom: 30px;
    }

    .form-group {
        text-align: left;
    }

    .ui-autocomplete-loading {
        background: white url("{{url('images/ui-anim_basic_16x16.gif')}}") right center no-repeat;
    }

    .form_error {
        color: red;
        font-size: 12px;
    }
</style>

@section('content')
    <div class="container flex-center full-height">
        <div class="row">
                
            <div class="col-sm-12">
                <div class="title m-b-md">
                    Register
                </div>
            
                <form method="POST" action="/register_user" name="register_form" id="register_form">
                    @csrf

                    <input type="hidden" name="county_name" id="county_name" value="">

                    <div class="register_form_view">
                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                
                                    @foreach ($errors->all() as $error)
                                        <p>{{ $error }}</p>
                                    @endforeach
                                
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <p>{{ session('status') }}</p>
                                
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <div class="row">
                            
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="user_name" id="user_name" maxlength="150" class="form-control" placeholder="eg. Dipika Tatale" />
                                        <p class="form_error user_name_error"></p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" name="user_email" id="user_email" maxlength="150" class="form-control" placeholder="eg. dipika@gmail.com" />
                                        <p class="form_error user_email_error"></p>
                                    </div>
                                </div>
                            
                        </div>
                        <div class="row">
                            
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phone">Phone</label>
                                        <input type="text" name="user_phone" id="user_phone" maxlength="10" class="form-control" placeholder="eg. 7850149036" onkeypress="return isNumber(event)" />
                                        <p class="form_error user_phone_error"></p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group ui-widget">
                                        <label for="street_address">Street Address</label>
                                        <input type="text" name="user_street_address" id="user_street_address" maxlength="250" class="form-control" placeholder="eg. 3214 1/2 Bunting Ave" />
                                        <p class="form_error user_street_address_error"></p>
                                    </div>
                                </div>
                            
                        </div>
                        <div class="row">
                            
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="city">City</label>
                                        <input type="text" name="user_city" id="user_city" maxlength="150" class="form-control" placeholder="eg. Clifton" />
                                        <p class="form_error user_city_error"></p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="state">State</label>
                                        <input type="text" name="user_state" id="user_state" maxlength="150" class="form-control" placeholder="eg. CO" />
                                        <p class="form_error user_state_error"></p>
                                    </div>
                                </div>
                            
                        </div>
                        <div class="row">
                            
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="zip">Zip</label>
                                        <input type="text" name="user_zip" id="user_zip" maxlength="10" class="form-control" placeholder="eg. 81520" onkeypress="return isNumber(event)" />
                                        <p class="form_error user_zip_error"></p>
                                    </div>
                                </div>
                            
                        </div>
                        <div class="row">
                            
                                <div class="col-sm-12">
                                    <button type="button" class="btn btn-success btn-sm register_user">Register</button>
                                </div>
                            
                        </div>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{asset('js/jquery-ui.js')}}"></script>
<script type="text/javascript">
    function isNumber(evt) 
    {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function validateEmail(textval) {
        var urlregex = new RegExp( "^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$");
        return urlregex.test(textval);
    }

    function validateNumber(textval) {
        var urlregex = new RegExp( "^[0-9]+$");
        return urlregex.test(textval);
    }

    $(document).ready(function(){
        $("input").attr('autocomplete', 'off');

        var street_address = '';

        //autocomplete
        $( "#user_street_address" ).autocomplete({
            source: "/user/search",
            minLength: 2,
            select: function( event, ui ) {
                console.log( "Selected: " + ui.item.value + ", " + ui.item.id );
                $("#user_street_address").val(ui.item.id);
                $("#user_city").val(ui.item.city);
                $("#user_state").val(ui.item.state);
                $("#user_zip").val(ui.item.zipcode);
                $("#county_name").val(ui.item.county_name);

                street_address = ui.item.id;
            },
            close: function( event, ui ) {
                $("#user_street_address").val(street_address);
            }
        });

        // validation
        $(document).on("click", ".register_user", function(){
            $(".form_error").html('');
            var form_error = true;

            if($.trim($("#user_name").val()) == "" || $.trim($("#user_name").val()) == undefined) {
                $(".user_name_error").html('Please enter Name');
                form_error = false;
            }

            if($.trim($("#user_email").val()) == "" || $.trim($("#user_email").val()) == undefined) {
                $(".user_email_error").html('Please enter Email');
                form_error = false;
            }  else if(!validateEmail($.trim($("#user_email").val()))) {
                $(".user_email_error").html('Please enter valid email address');
                form_error = false;
            }

            if($.trim($("#user_phone").val()) == "" || $.trim($("#user_phone").val()) == undefined) {
                $(".user_phone_error").html('Please enter Phone');
                form_error = false;
            }  else if(!validateNumber($.trim($("#user_phone").val()))) {
                $(".user_phone_error").html('Please enter digits only');
                form_error = false;
            }  else if($("#user_phone").val().length != 10 ) {
                $(".user_phone_error").html('Please enter 10 digits mobile number');
                form_error = false;
            } else if($("#user_phone").val().indexOf('0') == 0){
                $(".user_phone_error").html('Number cannot be started from 0');
                form_error = false;
            }

            if($.trim($("#user_street_address").val()) == "" || $.trim($("#user_street_address").val()) == undefined) {
                $(".user_street_address_error").html('Please enter Street Address');
                form_error = false;
            }

            if($.trim($("#user_city").val()) == "" || $.trim($("#user_city").val()) == undefined) {
                $(".user_city_error").html('Please enter City');
                form_error = false;
            }

            if($.trim($("#user_state").val()) == "" || $.trim($("#user_state").val()) == undefined) {
                $(".user_state_error").html('Please enter State');
                form_error = false;
            }

            if($.trim($("#user_zip").val()) == "" || $.trim($("#user_zip").val()) == undefined) {
                $(".user_zip_error").html('Please enter Zip');
                form_error = false;
            }  else if(!validateNumber($.trim($("#user_zip").val()))) {
                $(".user_zip_error").html('Please enter digits only');
                form_error = false;
            }  else if($("#user_zip").val().length != 5) {
                $(".user_zip_error").html('Please enter 5 digits Zip code');
                form_error = false;
            }

            if(form_error == true)
            {
                document.forms["register_form"].submit();
            }
        });
    });
</script>
@endpush
