<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;
use App\Events\UserRegistered;
use Illuminate\Http\Request;

use SmartyStreets;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $post_data = $request->all();

        $userData = User::create([
            'name' => $post_data['user_name'],
            'phone' => $post_data['user_phone'],
            'county_name' => $post_data['county_name'],
            'street_address' => $post_data['user_street_address'],
            'city' => $post_data['user_city'],
            'state' => $post_data['user_state'],
            'zip' => $post_data['user_zip'],
            'email' => $post_data['user_email'],
            'status' => 1
        ]);

        event(new UserRegistered($userData));

        //return view('register', ['message' => 'User has been registered successfully']);
        return redirect('/')->with('status', 'User has been registered successfully');
    }

    public function search(UserRequest $request)
    {
        $input_data = $request->all();

        if(!empty($input_data['term']) && trim($input_data['term']) != "")
        {
            $response = SmartyStreets::suggest(array(
                'prefix' => trim($input_data['term'])
            ));

            $suggestion_data_arr = [];

            if(isset($response['suggestions']))
            {
                foreach ($response['suggestions'] as $suggestion_data) {
                    $zipcode = '';
                    $county_name = '';

                    $response1 = SmartyStreets::addressQuickVerify(array(
                        'street' => $suggestion_data['street_line'],
                        'street2' => '',
                        'city' => $suggestion_data['city'],
                        'state' => $suggestion_data['state'],
                        'zipcode' => ''
                    ));

                    if(isset($response1) && count($response1) > 0)
                    {
                        if(isset($response1['components']) && count($response1['components']) > 0)
                        {
                            $zipcode = (isset($response1['components']['zipcode']) && !empty($response1['components']['zipcode'])) ? $response1['components']['zipcode'] : '';
                        }

                        if(isset($response1['metadata']) && count($response1['metadata']) > 0)
                        {
                            $county_name = (isset($response1['metadata']['county_name']) && !empty($response1['metadata']['county_name'])) ? $response1['metadata']['county_name'] : '';
                        }
                    }

                    array_push($suggestion_data_arr, ['id' => $suggestion_data['street_line'], 'value' => $suggestion_data['text']. ' ' .$zipcode, 'city' => $suggestion_data['city'], 'state' => $suggestion_data['state'], 'zipcode' => $zipcode, 'county_name' => $county_name]);
                }
            }

            return $suggestion_data_arr;
        }
        
        return json([]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
