<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [];

        if ($this->method() == "POST") {
            $rules = [
                'user_name' => ['required', 'string', 'max:255'],
                'user_email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
                'user_phone' => ['required', 'min:10', 'numeric', 'unique:users,phone'],
                'user_street_address' => ['required', 'string', 'max:255'],
                'user_city' => ['required', 'string', 'max:255'],
                'user_state' => ['required', 'string', 'max:255'],
                'user_zip' => ['required', 'min:5', 'numeric']
            ];
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'user_name.required' => 'Name is required',
            'user_email.required' => 'Email is required',
            'user_email.unique' => 'Email already exists',
            'user_phone.unique' => 'Phone already exists',
            'user_phone.required' => 'Phone is required',
            'user_street_address.required' => 'Street Address is required',
            'user_city.required' => 'City is required',
            'user_state.required' => 'State is required',
            'user_zip.required' => 'Zip is required',
        ];
    }
}
