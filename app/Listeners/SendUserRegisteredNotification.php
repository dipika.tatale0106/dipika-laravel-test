<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Log;
use App\User;
use Illuminate\Support\Facades\Mail;

class SendUserRegisteredNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $event->user  = (object) $event->user;
        Log::info("\nRegister User =>\n\nEmail-Id: " . $event->user->email);

        //send email
        $user_name = ucwords($event->user->name);
        $user_email = $event->user->email;

        $mail_data = ['name' => $user_name];

        Mail::send('email-templates/user_registered_email_notification', $mail_data, function ($message) use ($user_email, $user_name) {
            $message->to($user_email, $user_name)->subject('User Registered Notification');
            $message->from('dipika@laravel-test.com', 'Dipika Tatale');
        });
    }
}
